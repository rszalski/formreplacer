###
Author: Radosław Szalski

You need to buy a licence in order to use this script
http://codecanyon.net/item/formreplacer-fully-customize-your-form-elements/125515

This plugin replaces default checkboxes, selects and radioboxes
with more flexible and robust counterparts.

The best thing is, you can style those elements however you like!
That is because the new elements, are constructed using simple html elements
like spans and lists. These can be easily and uniformly styled across browsers!
Say goodbye to the pesky and inconsistent look.

More than that, it also provides an easy way to check/uncheck all checkboxes,
select a range (with shift-click) of options in Select-Multiple or Checkboxes.

Full keyboard navigation is supported, the elements will feel just like the native ones.

Each replaced element is thoroughly documented in the README. Check it out for more information :)
###
(($) ->
  $.fn.formReplacer = (options) ->
    # One var stmt
    
    ###
    Will hold last clicked checkbox for every checkbox group
    They need to be separate, in case of selecting more than one group
    one after another
    
    e.g. checkbox.lastClicked.name = $(obj)
    ###
    
    # KeyCodes for arrow keys
    
    # These are the only element types we act on and their event map
    evtRadioClick = (e) ->
      
      # Normal click event on radios
      e.preventDefault() # Just in case, see: checkbox click event
      # Deselect everything with the same name, except for the current event target
      $(".fReplacedRadio").filter(->
        $(this).data("name") is e.data.obj.newObj.data("name")
      ).removeClass "selected"
      $(":radio[name=\"" + e.data.obj.newObj.data("name") + "\"]").each ->
        $(this).prop "checked", false

      e.data.obj.old.filter("[value=\"" + e.data.obj.newObj.data("value") + "\"]").prop "checked", true
      e.data.obj.newObj.addClass "selected"
    evtRadioCheckboxKeypress = (e) ->
      
      # Keyboard Interaction - Space - used when navigating with tab
      #             * We need to use keypress, because opera doesn't support return false on keydown 
      e.which = e.which or e.keyCode
      if e.which is 32
        $(this).click()
        false
    
    ###
    Function that takes a jQuery object with checkboxes
    and automatically (de)selects connected old elements based on the passed elements
    
    @param elements - jQuery object with checkboxes to (de)select
    @param toggle - just switch selection
    @param select - select or deselect all
    ###
    selectCheckboxes = (elements, toggle, select) ->
      toCheck = undefined
      elements.each ->
        toCheck = (if (toggle) then not $(this).hasClass("selected") else select)
        $(this).toggleClass "selected", toCheck
        $(":checkbox[name=\"" + $(this).data("name") + "\"][value=\"" + $(this).data("value") + "\"]").prop "checked", toCheck

    evtCheckboxClick = (e) ->
      e.preventDefault() # Important, since labels with for attr will also respond to this event
      cancelOutline $(this) # When clicking with mouse, we cancel outline
      sameNameCheckboxes = $(".fReplacedCheckbox").filter(->
        $(this).data("name") is e.data.obj.nameAttr and not $(this).data("disabled")
      )
      thisIndex = sameNameCheckboxes.index($(this))
      
      # We make note of every unique group of checkboxes we are operating on.
      #                We then add them to lastClicked.checkbox and mark lastClicked as null 
      checkbox.lastClicked[e.data.obj.nameAttr] = null  unless checkbox.lastClicked[e.data.obj.nameAttr]
      
      ###
      Shift-Click Event
      We only touch checkboxes with the same name attribute!
      ###
      if e.shiftKey
        if checkbox.lastClicked[e.data.obj.nameAttr] is null
          selectCheckboxes sameNameCheckboxes.filter(":lt(" + thisIndex + ")").andSelf(), false, true
          selectCheckboxes sameNameCheckboxes.filter(":gt(" + thisIndex + ")"), false, false
        else
          lastClickedIndex = sameNameCheckboxes.index(checkbox.lastClicked[e.data.obj.nameAttr])
          start = (if (lastClickedIndex <= thisIndex) then lastClickedIndex else thisIndex)
          end = (if (lastClickedIndex <= thisIndex) then thisIndex + 1 else lastClickedIndex + 1)
          checkboxRange = sameNameCheckboxes.slice(start, end)
          selectCheckboxes checkboxRange, false, true # Select the chosen range
          selectCheckboxes sameNameCheckboxes.not(checkboxRange), false, false # Deselect everything else
      else # Normal Click - move along...
        checkbox.lastClicked[e.data.obj.nameAttr] = $(this)
        selectCheckboxes $(this), true, false
    
    ###
    Extends the obj with properties specific to Checkboxes and Radios
    ###
    createCheckboxRadio = (obj) ->
      obj.newObj = $("<span class=\"fReplaced" + obj.className + " " + obj.existingClasses + "\" id=\"" + obj.existingIDs + "\" tabindex=\"" + assignTabIndex(obj.old) + "\"></span>")
      obj.elemImgObj = $("<span class=\"fReplaced" + obj.className + "Img\"></span>")
      
      # 'Name' allows us to recognize elements from the same group
      obj.newObj.data "name", obj.nameAttr
      
      # Value is used to identify corresponding Old Elements e.g. in shift-click
      obj.newObj.data "value", obj.old.prop("value")
      
      # Is the element disabled ?
      obj.newObj.data "disabled", obj.old.is(":disabled")
      
      # Make sure already selected/checked elements have the new, .selected class
      obj.newObj.toggleClass "selected", obj.old.is(":checked")
      
      # If the element is disabled
      obj.newObj.toggleClass "disabled", obj.newObj.data("disabled")
      
      # The order is important, don't mix it up, since it can later mess up the event bindings
      obj.old.after obj.newObj
      obj.newObj.append(obj.elemImgObj).append obj.label
    evtSelectClick = (e) ->
      e.stopPropagation()
      cancelOutline $(this) # When clicking with mouse, we don't want outline too
      ###
      If RTL is true, we need to match $newOptions' right corner to newElem's right corner
      Otherwise, just left corners
      ###
      e.data.obj.newOptions.css
        position: "absolute"
        top: e.data.obj.newObj.offset().top + e.data.obj.newObj.outerHeight() - 1
        left: (if (opts.rtl) then e.data.obj.newObj.offset().left + e.data.obj.newObj.outerWidth() - e.data.obj.newOptions.outerWidth() else e.data.obj.newObj.offset().left)
        zIndex: "9999" # Z-Index is added here, because it's crucial to UL's behavior

      e.data.obj.newOptions.slideToggle()
    evtSelectKeydown = (e) ->
      
      ###
      Selects - Keyboard navigation
      
      If the element is focused, arrows change selection
      Left (37) & Up (38) - select previous option
      Right (39) & Down (40) - select next option
      
      Remember to only cancel default action when pressing arrow keys!
      ###
      lis = e.data.obj.newOptions.children()
      liCount = lis.length
      curLi = lis.filter(->
        $(this).data("value") is e.data.obj.old.val()
      )
      e.which = e.which or e.keyCode
      if e.which is arrow.right or e.which is arrow.down
        
        # We are essentially clicking on the next option
        nextLi = curLi.next()
        if (lis.index(curLi) + 1) isnt liCount
          
          # Skipping optGroups and disabled options
          nextLi = nextLi.next()  while nextLi.data("disabled") or nextLi.hasClass("fReplacedOptGroup")
          nextLi.click()
        false
      else if e.which is arrow.left or e.which is arrow.up
        
        # We are essentially clicking on the prev option
        prevLi = curLi.prev()
        if lis.index(curLi) isnt 0
          
          # Skipping optGroups and all the disabled options
          prevLi = prevLi.prev()  while prevLi.data("disabled") or prevLi.hasClass("fReplacedOptGroup")
          prevLi.click()
        false
    
    ###
    Handles clicking outside the Select element, hides the options if so
    ###
    evtSelectDocumentClick = (e) ->
      $(".fReplacedSelectOptions").slideUp()  unless $(e.target).parents(".fReplacedSelectOptions").length
    
    ###
    Extends the obj with properties specific to Selects
    ###
    createSelect = (obj) ->
      obj.newObj = $("<span class=\"fReplacedSelect" + " " + obj.existingClasses + "\" id=\"" + obj.existingIDs + "\" tabindex=\"" + assignTabIndex(obj.old) + "\"></span>")
      obj.options = obj.old.children("option")
      obj.hasOptGroups = !!obj.old.has("optgroup").length
      obj.optGroups = (if (obj.hasOptGroups) then obj.old.children("optgroup") else null)
      obj.currentOption = (if (obj.hasOptGroups) then obj.old.find("option:selected").text() else obj.options.filter(":selected").text())
      obj.newOptions = $("<ul class=\"fReplacedSelectOptions\"></ul>").hide()
      
      # For Identification purposes
      obj.newObj.data "name", obj.nameAttr
      obj.newOptions.data "name", obj.nameAttr
      
      # Is the element disabled ?
      obj.newObj.data "disabled", obj.old.is(":disabled")
      obj.newObj.toggleClass "disabled", obj.newObj.data("disabled")
      unless obj.newObj.data("disabled")
        
        # This is bound only once, hides options if the click occurs outside of Select
        $(document).off "click", evtSelectDocumentClick
        $(document).on "click",
          obj: obj
        , evtSelectDocumentClick
        
        ###
        Main click event on options
        Remember to suppress Opt-groups
        ###
        obj.newOptions.delegate "li", "click", (e) ->
          if not $(e.target).hasClass("fReplacedOptGroup") and not $(e.target).data("disabled")
            obj.newOptions.slideUp()
            obj.newObj.html $(this).html()
            obj.old.val $(this).data("value")

      
      ###
      We append options ( as LI items) to UL, values are stored via .data()
      
      We need to take into account the possibility, that values will be empty,
      if so, we insert &nbsp; to keep the dimensions intact
      ###
      if obj.hasOptGroups
        obj.optGroups.each ->
          optGroup = $("<li id=\"" + $(this).attr("id") + "\" class=\"fReplacedOptGroup" + " " + $(this).attr("class") + "\">" + $(this).attr("label") + "</li>")
          obj.newOptions.append optGroup
          $(this).children("option").each (i, option) ->
            text = (if ($(option).text() isnt "") then $(option).text() else "&nbsp;")
            classes = $(option).attr("class")
            disabledClass = (if $(option).is(":disabled") then "disabled" else "")
            
            # Disabled lis have the class and data set accordingly and we copy exiting classes
            li = $("<li/>",
              class: classes + " " + disabledClass
              id: $(option).attr("id")
              text: text
            ).data("value", $(option).val()).data("disabled", $(option).is(":disabled"))
            obj.newOptions.append li

      else
        obj.options.each (i, option) ->
          text = (if ($(option).text() isnt "") then $(option).text() else "&nbsp;")
          classes = $(option).attr("class")
          disabledClass = (if $(option).is(":disabled") then "disabled" else "")
          li = $("<li/>",
            class: classes + " " + disabledClass
            id: $(option).attr("id")
            text: text
          ).data("value", $(option).val()).data("disabled", $(option).is(":disabled"))
          obj.newOptions.append li

      
      ###
      IE6 Specific Code
      We need to assign calculated (with units) width to LIs,
      otherwise IE6 will only interpret click() on text nodes, not the whole LI
      (default width is 'auto')
      
      Yes, I also don't like browser sniffing.
      ###
      obj.newOptions.children("li").css "width", obj.newOptions.width()  if parseInt(jQuery.browser.version, 10) <= 6  if jQuery.browser.msie
      
      # In case the option text is empty, insert dummy &nbsp;
      value = (if (obj.currentOption isnt "") then obj.currentOption else "&nbsp;")
      obj.newObj.html(value).insertAfter(obj.old).after obj.newOptions
    
    ###
    Select Multiple - Keyboard Navigation
    
    Actions:
    - arrow up/down - selects only 1 element (deselects others), next or previous
    - arrow up/down + shift - selects a range from current to arrow destination (like normal shift-click)
    but when the arrow direction changed we decrement selection by one (not "mirroring" like normally)
    - arrow + ctrl - we move an outline from currently selected li to arrow destination,
    which then can be toggled with space
    ###
    evtSelectMultipleKeydown = (e) ->
      cancelTextSelection()
      e.which = e.which or e.keyCode
      event = $.Event("click")
      currentlySelected = $(this).children("li").filter(->
        e.data.obj.old.children("option[value=\"" + $(this).data("value") + "\"]").is ":selected"
      )
      
      # For Simulating shift-click
      event.shiftKey = true
      
      ###
      Space Event
      ###
      if e.which is 32
        toggleSelection $(this).children("li").filter(->
          $(this).data("outlined") is true
        ), e.data.obj.old
        return false
      if e.which is arrow.right or e.which is arrow.down
        
        # We either move with selection down from the last selected item, or select the first one (moving down from 0 el)
        next = (if (currentlySelected.length is 0) then $(this).children("li:first") else currentlySelected.filter(":last").next())
        
        # If the next item is disabled, we need to select the next-to-the-next one
        next = next.next()  while next.data("disabled")
        
        ###
        Shift-Key
        
        Behaves like a normal shift-click for now.
        ###
        if e.shiftKey
          
          ###
          If the position is positive, we need to "step back" one selection after another
          This is the native behaviour. Otherwise proceed as usual while shift-clicking.
          ###
          if position > 0
            toggleSelection currentlySelected.filter(":first"), e.data.obj.old
          else
            next.trigger event
          
          # We need to stop decrementing if we hit the last element while selecting
          position--  unless currentlySelected.filter($(this).children("li:not(.disabled):last")).length
        else if e.ctrlKey
          cancelOutline $(this).children("li")
          currIndex = 0  if currIndex is null
          $(this).children("li:not(.disabled)").eq(currIndex).data("outlined", true).css "outline", "#fff dotted thin"
          currIndex = (if (currIndex < $(this).children("li:not(.disabled)").length - 1) then ++currIndex else currIndex)
        else
          
          # Also cancels outline
          cancelOutline $(this).children("li")
          
          # Each normal click restores the position to 0 (neutral)
          position = 0
          
          # We need to stop going down, if we hit the last element
          # Remember to also count disabled elements
          if $(this).children("li:not(.disabled)").index(currentlySelected.filter(":last")) isnt ($(this).children("li:not(.disabled)").length - 1)
            next.click()
            currIndex = $(this).children("li:not(.disabled)").index(next)
          else
            $(this).children("li:not(.disabled):last").click()
            currIndex = $(this).children("li:not(.disabled)").length - 1
        false
      else if e.which is arrow.left or e.which is arrow.up
        
        # We either move up with selection from the last selected item, or select the first item (the native way)
        prev = (if (currentlySelected.length is 0) then $(this).children("li:first") else currentlySelected.filter(":first").prev())
        
        # Skipping the disabled LIs
        prev = prev.prev()  while prev.data("disabled")
        if e.shiftKey
          
          ###
          If the position is negative we need to "back up" one selection after another
          This is the native behaviour. Otherwise proceed as usual while shift-clicking.
          ###
          if position < 0
            toggleSelection currentlySelected.filter(":last"), e.data.obj.old
          else
            prev.trigger event
          
          # We need to stop incrementing if we hit the first item when selecting
          position++  unless currentlySelected.filter($(this).children("li:not(.disabled):first")).length
        else if e.ctrlKey
          position++
          currIndex = (if (currIndex > 0) then --currIndex else currIndex)
          cancelOutline $(this).children("li")
          $(this).children("li:not(.disabled)").eq(currIndex).data("outlined", true).css "outline", "#fff dotted thin"
        else
          
          # Also cancels outline
          cancelOutline $(this).children("li")
          
          # Normal click restores the position to 0
          position = 0
          if $(this).children("li:not(.disabled)").index(currentlySelected.filter(":first")) isnt 0
            prev.click()
            currIndex = $(this).children("li:not(.disabled)").index(prev)
          else
            $(this).children("li:not(.disabled):first").click()
            
            # We set currentIndex to 1 as the second element will be the one to get outlined next (if click down later)
            currIndex = $(this).children("li:not(.disabled)").index("li:not(.disabled):first")
        false
    
    ###
    Extends the obj with properties specific to Select Multiple
    ###
    createSelectMultiple = (obj) ->
      
      ###
      Replaces select-multiple, a type of select
      that has all the options shown (no sliding) and
      allows for checking many options at the same time.
      ###
      obj.newObj = $("<ul id=\"" + obj.existingIDs + "\" class=\"fReplacedSelectMultiple" + " " + obj.existingClasses + "\" tabindex=\"" + assignTabIndex(obj.old) + "\"></ul>").data("name", obj.old.attr("name"))
      
      # Is the element disabled ?
      obj.newObj.data "disabled", obj.old.is(":disabled")
      obj.newObj.toggleClass "disabled", obj.newObj.data("disabled")
      obj.options = obj.old.children("option")
      obj.selectedOptions = obj.old.children("option:selected")
      obj.lastClicked = null # Important for shift-click functionality,
#                                            so we can determine the range
#                                            IMPORTANT: each selectmultiple has its own set of vars, this is fine here,
#                                            but in case of checkboxes, you need a global one!
      obj.options.each (i, option) ->
        text = (if ($(option).text() isnt "") then $(option).text() else "&nbsp;")
        li = $("<li/>",
          class: $(option).attr("class")
          id: $(option).attr("id")
          text: text
        ).data("value", $(option).val()).data("disabled", $(option).is(":disabled"))
        li.toggleClass "selected", $(option).is(":selected")
        li.toggleClass "disabled", li.data("disabled")
        obj.newObj.append li

      unless obj.newObj.data("disabled")
        
        ###
        Each LI will handle either a normal click or a shift-click.
        
        Normal click behaves exactly like that of a native select-multiple,
        where it un-selects previously selected options (or needs a CTRL to select >1)
        ###
        obj.newObj.delegate "li", "click", (e) ->
          unless $(e.target).data("disabled")
            
            # When clicking with mouse, we don't want outline
            cancelOutline obj.newObj
            
            ###
            Shift-click should mimic native behavior, that is
            selects a range from last clicked option, to currently clicked one.
            
            We can select in both directions "forward" and "backward", depending on indexes relation.
            ###
            if e.shiftKey
              
              ###
              This click is the first one, so we select whole range,
              from beginning up to the current position.
              
              .andSelf() ensures we add currently clicked element to the set as well
              ###
              if obj.lastClicked is null
                
                # We need to filter out the disabled elements
                obj.old.children("option[value=\"" + $(this).data("value") + "\"]").prevAll().andSelf().filter(->
                  $(this).prop("disabled") is false
                ).prop "selected", true
                $(this).prevAll().andSelf().filter(->
                  $(this).data("disabled") is false
                ).addClass "selected"
                obj.old.children("option[value=\"" + $(this).data("value") + "\"]").nextAll().prop "selected", false
                $(this).nextAll().removeClass "selected"
              else
                
                ###
                Previous click was a normal (non-shift) click.
                
                Depending on indexes of current $(this) and last click we determine the range to act on.
                ###
                start = (if (obj.lastClicked.index() <= $(this).index()) then obj.lastClicked.index() else $(this).index())
                end = (if (obj.lastClicked.index() <= $(this).index()) then $(this).index() + 1 else obj.lastClicked.index() + 1)
                liRange = $(this).siblings().andSelf().slice(start, end)
                optionRange = obj.old.children("option").slice(start, end)
                
                # We select everything in range, but we need to filter out the disabled options
                optionRange.filter(->
                  $(this).prop("disabled") is false
                ).prop "selected", true
                liRange.filter(->
                  $(this).data("disabled") is false
                ).addClass "selected"
                
                # And de-select everything else
                obj.old.children("option").not(optionRange).prop "selected", false
                $(this).siblings().not(liRange).removeClass "selected"
            else # Normal Click - move along...
              obj.lastClicked = $(this) # Marks the current click, so it can be used when determining ranges in shift-click
              oldOption = obj.old.children("option[value=\"" + $(this).data("value") + "\"]")
              
              ###
              Holding CTRL allows us to select more than 1 option
              If it's not pressed, then we deselect everything other than "current e.target"
              ###
              if e.ctrlKey
                selected = oldOption.is(":selected")
                oldOption.prop "selected", not selected
                $(this).toggleClass "selected", not selected
              else
                oldOption.prop "selected", true
                $(this).addClass "selected"
                oldOption.siblings().prop "selected", false
                $(this).siblings().removeClass "selected"

      obj.newObj.insertAfter obj.old
      
      ###
      We are fixing a situation, when in Chrome and IE7 scrollbar is added inside the ul.
      
      This results in an unwelcome horizontal scrollbar. Since the standard width of a scrollbar is 20px
      we need to add 20px to the actual width, so horizontal bar is not needed
      ###
      obj.newObj.css "width", obj.newObj.width() + 20
    
    # Factory method, provides unified constructor for all the supported elements
    
    # We are preserving the classes and IDs
    
    # We are extending the element with specific settings
    
    # Binding every specified event
    
    # Main loop
    
    ###
    Cancels any selection that is currently in progress
    ###
    cancelTextSelection = ->
      if $.browser.msie
        document.selection.empty() # This works only in IE, everywhere else it breaks this code
      
        ###
      Removing all ranges in opera 9.6 makes the element
      completely lose focus. We can delete the range
      and then set focus back to the right element
      but the effect is the same - everything is selected.
      So we do nothing.
        ###
      else window.getSelection().removeAllRanges()  unless $.browser.opera and parseInt($.browser.version, 10) is 9
    preventTextSelection = (e) ->
      result = e.shiftKey or e.ctrlKey
      cancelTextSelection()  if result
      @onselectstart = -> # IE
        not result

      not result # Normal browsers
    
    ###
    This is for Opera, return false in keydown is not supported
    We only 'cancel' arrow keys and space
    ###
    operaPreventDefault = (e) ->
      e.which = e.which or e.keyCode
      false  if e.which is 32 or e.which is arrow.up or e.which is arrow.down or e.which is arrow.right or e.which is arrow.left
    
    ###
    Utility Functions
    
    cancelOutline - cancels element's outline
    handleoutline
    assignTabindex - ensures consistent and copied tabindexes
    toggleSelection - only used by Select Multiple
    ###
    cancelOutline = (elem) ->
      elem.css("outline", "none").data "outlined", false
    
    ###
    Ensures we only get outline when using keyboard navigation
    Outline is neither necessary nor pretty when clicking with mouse
    
    @param elem - jQuery Obj - element for which we want to handle outline
    ###
    handleOutline = (elem) ->
      
      # We remove the outline by default and apply it on keyup()!
      elem.css "outline", "none"
      
      # We lose focus, we lose outline.
      elem.focusout ->
        $(this).css "outline", "none"

      
      # By default, outline is 'none' but it has to be applied
      #             * if we focus elements with tab! 
      elem.keyup (e) ->
        e.which = e.which or e.keyCode
        $(this).css "outline", "#fff dotted thin"  if e.which is 9

    
    ###
    Tabindex is carried over from the old element to the new
    Consecutive indexes are incremented accordingly to the last one
    ###
    assignTabIndex = (el) ->
      defaults.tabIndex = (if (el.attr("tabindex")) then el.attr("tabindex") else ++defaults.prevTabIndex)
      defaults.prevTabIndex = defaults.tabIndex
      defaults.tabIndex
    toggleSelection = (elem, oldElem) ->
      elem.each ->
        if oldElem.children("option[value=\"" + elem.data("value") + "\"]").prop("selected") is true
          elem.removeClass "selected"
          oldElem.children("option[value=\"" + elem.data("value") + "\"]").prop "selected", false
        else
          elem.addClass "selected"
          oldElem.children("option[value=\"" + elem.data("value") + "\"]").prop "selected", true

    defaultOptions = rtl: false
    opts = $.extend(defaultOptions, options)
    defaults =
      tabIndex: 0
      prevTabIndex: 0

    checkbox =
      lastClicked: {}
      checkAll: true

    arrow =
      left: 37
      up: 38
      right: 39
      down: 40

    supportedTypes =
      radio:
        create: createCheckboxRadio
        events:
          click: evtRadioClick
          keypress: evtRadioCheckboxKeypress

      checkbox:
        create: createCheckboxRadio
        events:
          click: evtCheckboxClick
          mousedown: preventTextSelection
          keypress: evtRadioCheckboxKeypress

      "select-one":
        create: createSelect
        events:
          click: evtSelectClick
          keydown: evtSelectKeydown
          keypress: operaPreventDefault

      "select-multiple":
        create: createSelectMultiple
        events:
          mousedown: preventTextSelection
          keypress: operaPreventDefault
          focus: preventTextSelection
          keydown: evtSelectMultipleKeydown

    position = 0
    currIndex = null
    class Element
      constructor: (type, oldElement) ->
        @className = type.replace(/^./, type.match(/^./)[0].toUpperCase())
        @elemType = type
        @old = $(oldElement).hide()
        @existingClasses = (if $(oldElement).attr("class") then $(oldElement).attr("class") else "")
        @existingIDs = (if $(oldElement).attr("id") then $(oldElement).attr("id") else "")
        @nameAttr = $(@old).attr("name")
        @label = $("label[for=\"" + $(@old).attr("id") + "\"]")
        supportedTypes[@elemType].create this
        unless @newObj.data("disabled")
          @newObj.on supportedTypes[@elemType]["events"],
            obj: this

    @each ->
      if @type of supportedTypes
        elem = new Element(@type, @)
        handleOutline elem.newObj

    
    ###
    Check/Uncheck All
    
    Every element with a class 'checkAll' will toggle selected state
    for those checkboxes, which name is also included as a class
    ###
    $(".checkAll").click ->
      
      # Storing every class except .checkAll
      names = $(this).attr("class").replace(/(checkAll\s)(.*)/, "$2").split(" ")
      affected = 0
      checked = 0
      sameNameGroup = null
      for name of names
        
        ###
        We are counting the total number of affected as well as checked checkboxes
        And caching checkboxes with the same name
        
        We should also filter out the disabled
        elements.
        ###
        sameNameGroup = $(".fReplacedCheckbox").filter(->
          $(this).data("name") is names[name] and not $(this).hasClass("disabled")
        )
        sameNameGroup.each ->
          affected++
          checked++  if $(":checkbox[value=\"" + $(this).data("value") + "\"]").is(":checked")

        
        ###
        These are the 2 edge cases when our checkAll flag might get stuck.
        This happens if we have more than 1 group of checkboxes:
        
        1st is fully selected, making checkAll = false, the 2nd one is deselected
        if we try to select the 2nd one, we wouldn't be able because the flag is already set to false
        
        Same when checkAll = true and we want to select a completely deselected group.
        
        The solution is to switch the flag again, every time this happens
        ###
        checkbox.checkAll = false  if (affected is checked) and checkbox.checkAll
        checkbox.checkAll = true  if (checked is 0) and not checkbox.checkAll
        sameNameGroup.each ->
          $(this).toggleClass "selected", checkbox.checkAll
          $(":checkbox[value=\"" + $(this).data("value") + "\"]").prop "checked", checkbox.checkAll

      checkbox.checkAll = not checkbox.checkAll
      false # In case the element with .checkAll class has a default action

    this
) jQuery