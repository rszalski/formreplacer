describe 'Checkboxes', ->
    mainSpan = {}
    imageSpan = {}

    # Events
    keypressWithSpace = {}

    beforeEach ->
        recreateEvents()

    recreateEvents = ->
        # We need to recreate events every time, jQuery bug
        keypressWithSpace = $.Event('keypress')
        keypressWithSpace.which = 32

    describe 'Single checkbox', ->
        beforeEach ->
            loadFixtures 'singleCheckbox.html'
            $(':checkbox').formReplacer()

            mainSpan = $('#jasmine-fixtures > span')
            imageSpan = $('#jasmine-fixtures > span span')

        it 'should not remove native checkbox', ->
            expect( $(':checkbox').length ).toEqual 1

        it 'should hide native checkbox', ->
            expect($(':checkbox').is(':hidden')).toBe true

        it 'should create replacer spans', ->
            expect( $('#jasmine-fixtures span').length).toEqual 2

        it 'should contain image span inside main span', ->
            expect( $('#jasmine-fixtures > span:has(span)').length ).toEqual 1

        describe 'Main span', ->
            it 'should have `fReplacedCheckbox` class ', ->
                expect( mainSpan.hasClass('fReplacedCheckbox') ).toBe true

            it 'should have the same id set', ->
                expect( mainSpan.attr('id') ).toEqual 'bike'

            it 'should have the same tabindex set', ->
                expect( mainSpan.attr('tabindex') ).toEqual 5

            it 'should have correct html data assigned', ->
                # TODO split this to 5 different expects?
                element = mainSpan[0]

                expect( $.hasData(element) ).toBe true
                expect( mainSpan.data('disabled') ).toBe false    
                expect( mainSpan.data('name') ).toEqual 'vehicle[]'
                expect( mainSpan.data('value') ).toEqual 'bike'

        describe 'Image span', ->
            it 'should have `fReplacedCheckboxImg` class', ->
                expect( imageSpan.hasClass('fReplacedCheckboxImg') ).toBe true

            it 'should be empty', ->
                expect( imageSpan.length ).toEqual 1
                expect( imageSpan.children().length ).toEqual 0

            it 'should not have any html data bound', ->
                expect( imageSpan.length ).toEqual 1
                expect( $.hasData(imageSpan) ).toBe false

        describe 'when clicked on with a mouse', ->
            it 'should assign or remove `selected` class', ->
                expect( mainSpan ).not.toHaveClass 'selected'

                # Select
                mainSpan.trigger 'click' 
                expect( mainSpan ).toHaveClass 'selected'

                # Deselect
                mainSpan.trigger 'click' 
                expect( mainSpan ).not.toHaveClass 'selected'

        describe 'when selected with keyboard', ->
            it 'should assign or remove `selected` class', ->
                expect( mainSpan ).not.toHaveClass 'selected'

                # Select with keyboard
                mainSpan.trigger keypressWithSpace
                expect( mainSpan ).toHaveClass 'selected'

                recreateEvents()

                # Deselect with keyboard
                mainSpan.trigger keypressWithSpace
                expect( mainSpan ).not.toHaveClass 'selected'


    describe 'Single disabled checkbox', ->
        beforeEach ->
            loadFixtures 'singleDisabledCheckbox.html'
            $(':checkbox').formReplacer()

            mainSpan = $('#jasmine-fixtures > span')

        it 'should have a `disabled` class assigned', ->
            expect( mainSpan.hasClass('disabled') ).toBe true

        it 'should have correct html data assigned', ->
            domElement = mainSpan[0]

            expect( $.hasData(domElement) ).toBe true
            expect( mainSpan.data('disabled') ).toBe true    

        it 'when clicked on with a mouse should not change `selected` class', ->
            expect( mainSpan ).not.toHaveClass 'selected'

            # Try to select
            mainSpan.trigger 'click' 
            expect( mainSpan ).not.toHaveClass 'selected'

            # Try to deselect
            mainSpan.trigger 'click' 
            expect( mainSpan ).not.toHaveClass 'selected'

    describe 'Single disabled, already selected checkbox', ->
        beforeEach ->
            loadFixtures 'singleDisabledAlreadySelectedCheckbox.html'
            $(':checkbox').formReplacer()

            mainSpan = $('#jasmine-fixtures > span')

        it 'when clicked on with a mouse should not change `selected` class', ->
            expect( mainSpan ).toHaveClass 'selected'

            # Try to select
            mainSpan.trigger 'click' 
            expect( mainSpan ).toHaveClass 'selected'

            # Try to deselect
            mainSpan.trigger 'click' 
            expect( mainSpan ).toHaveClass 'selected'

    describe 'Single checkbox with label next to it', ->
        beforeEach ->
            loadFixtures 'singleCheckboxWithLabelNextToIt.html'
            $(':checkbox').formReplacer()

            mainSpan = $('#jasmine-fixtures > span')

        it 'should use and nest provided label', ->
            expect( mainSpan.children('label').length ).toEqual 1

    describe 'Single already selected checkbox', ->
        beforeEach ->
            loadFixtures 'singleAlreadySelectedCheckbox.html'
            $(':checkbox').formReplacer()

            mainSpan = $('#jasmine-fixtures > span')

        it 'should have `selected` class assigned', ->
            expect( mainSpan ).toHaveClass 'selected'

        it 'when clicked on with a mouse for the first time should remove `selected` class', ->
            expect( mainSpan ).toHaveClass 'selected'

            mainSpan.trigger 'click'
            expect( mainSpan ).not.toHaveClass 'selected'

        it 'when selected with a keyboard for the first time should remove `selected` class', ->
            expect( mainSpan ).toHaveClass 'selected'

            mainSpan.trigger keypressWithSpace
            expect( mainSpan ).not.toHaveClass 'selected'

        it 'when clicked on with a mouse twice should assign `selected` class', ->
            expect( mainSpan ).toHaveClass 'selected'

            mainSpan.trigger 'click'
            expect( mainSpan ).not.toHaveClass 'selected'

            mainSpan.trigger 'click'
            expect( mainSpan ).toHaveClass 'selected'

        it 'when selected with a keyboard twice should assign `selected` class', ->
            expect( mainSpan ).toHaveClass 'selected'

            mainSpan.trigger keypressWithSpace
            expect( mainSpan ).not.toHaveClass 'selected'

            recreateEvents()

            mainSpan.trigger keypressWithSpace
            expect( mainSpan ).toHaveClass 'selected'
            
