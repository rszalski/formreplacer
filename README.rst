*************
* Changelog *
*************

Version 1.1.2 
        - formReplacer has been updated and tested with jQuery 1.7.1
        - IMPORTANT! At least jQuery 1.7.0 is required!

Version 1.1.1 
        - Classes and IDs are now correctly carried over from the original elements to the replaced ones

Version 1.1.0
added:
	- Full Keyboard Navigation:
		This release was focused on accessibility, so now keyboard is fully supported on the replaced elements.
		You can select them with tab and use keys! (see 'Keyboard Navigation' in 'Additional Functionality' section)

	- Support for disabled elements:
		Be it whole inputs or just options, if you put disabled="disabled" attribute on them, they will be respected.
		They get special styles an no actions can be performed on them.

	- RTL Support:
		It works like this: put dir="rtl" on BODY tag, include style.rtl.css AFTER style.css
		pass rtl: true as a parameter to formReplacer() - And it Works! (see 'RTL' in 'Additional Functionality' section)

	- Shift-click on checkboxes:
		You can now select a whole range of checkboxes, by holding shift!


fixed:
	- Shift click events will no longer select/highlight text
	- Major code optimization and improvements


Version 1.0.1 - First Release


***************
* Description *
***************

What does it do and how ?

	This plugin completely replaces the ugly and inflexible form elements with better counterparts.
	The old ones are still there, but hidden. Every action you take on the new elements, are carried over to the 'old' ones.
	This way the internal functionality (form submitting, $_GET/$_POST variables) is fully preserved.

	This allows for extremely easy customization (see 'Customization' section).

Additional features

	Graceful Degradation - users without JavaScript enabled will get the old elements, and won't notice a thing :)

	Apart from making form elements flexible, another goal was to improve user experience.
	This is done with many simple things:

	• enabling keyboard interaction - same as on the original elements!
	• enabling hover states over labels - it clearly indicates the element is click-able,
	• making label, element and the space IN BETWEEN trigger the element - by supplying 'for' attribute on label
		you make the label click-able even without js but with this plugin this is much more flexible as you can set
		whatever dimensions and functionality you like (see 'Anatomies' section) to the elements.

	This plugin also supports often requested Check/Uncheck All checkboxes functionality (see 'Check/Uncheck All' section),
	as well as Shift-Click on Select-Multiples and Checkboxes - this way you can easily select options you want.

****************
* Installation *
****************

Simply include the script in your web page like so:

<script type="text/javascript" src="jquery.formReplacer.min.js"></script>

Notes:
	- You need jQuery for this plugin to work, get it here: http://jquery.com/ .
	  (Make sure to include jQuery before including this plugin!),
	- If you are using HTML5 you can omit the 'type' attribute - yay for streamlined code!,
	- .min file is a minified version of the script, recommended for production.

*********
* Usage *
*********

You just need to pass the form elements you want to change in a jquery selector
and invoke formReplacer() on them.

Example:
	This will replace all possible elements
	$(':checkbox, :radio, select').formReplacer();

You can use them in any order or combination:

	This will only replace select elements
	$('select').formReplacer();

	This will replace only radios inside div with a class fancy
	$('div.fancy :radio').formReplacer();

The element selectors are as follows:
:checkbox   - all checkbox elements
:radio      - all radio elements
select      - both select - ones and select - multiples
              (difference is described in 'Anatomy of a Select' section)


*****************
* Customization *
*****************

Customization

	As mentioned earlier, customization is very easy because the elements are basically constructed from scratch,
	using flexible html elements like SPANs and ULs. This way, you only need a .css stylesheet to
	completely and uniformly change the elements appearance

	Styles are thoroughly documented in the css file. In the following section I will describe
	how each element is constructed and how it functions, so you know exactly what to change.


*************************
* Anatomy of a Checkbox *
*************************

HTML code - Old

	<input type="checkbox" name="someName" id="car" value="car" />
	<label for="car">Car</label>

	Note:   Label's 'for' attribute matches input's 'id' attribute.
			You should always include labels with 'for' attributes.

								| |
								| |
								V V

HTML code - Replaced

	<span class="fReplacedCheckbox">                 <-------------- The container span that handles clicking
		<span class="fReplacedCheckboxImg"></span>   <-------------- This is a span that has the image
		<label for="car">Car</label>                 <-------------- Label with for attribute
	</span>

Once the checkbox is clicked/checked a 'selected' class is added. This way you can customize everything with css.

	Selected checkbox

	<span class="fReplacedCheckbox selected">
		(...)
	</span>

If the original element has disabled="disabled" attribute, the class .disabled is appended
	Disabled checkbox

	<span class="fReplacedCheckbox disabled">
		(...)
	</span>

Handling Clicks

	Click events are attached to the topmost element, which is .fReplacedCheckbox - that way whole area is click-able,
	no matter how you style labels inside, or what dimensions the .fReplacedCheckbox has.

	Want the click-able area bigger? Just alter css and add padding or make it wider!

	If the element is disabled - no clicks for him!

jQuery Data

	Each checkbox belonging to the same group (this is, having the same 'name' attribute) has data object attached to it
	.data('name') = name attribute
	.data('disabled) = does it have disabled attribute ?

	This is used by the plugin internally, but you can also use this by yourself. Let's say you want to select
	every NEW checkbox and change their appearance with jQuery - you can you the filter method:

	This will hide every new (replaced) checkbox that has the name attribute of 'someName':

	$('.fReplacedCheckbox').filter(function() {
		return $(this).data('name') === 'someName';
	}).hide();

	Note:
		- Do NOT change names/values inside .data()! - this will break most of the plugin!

	

*************************
* Anatomy of a Radiobox *
*************************

Radioboxes are very similar in structure to checkboxes, only class names change a bit :)

HTML code - Old

	<input type="radio" name="article" id="delete" value="delete" />
	<label for="delete">Delete</label>

	Note: Label's 'for' attribute matches input's 'id' attribute.

								| |
								| |
								V V

HTML code - Replaced

	<span class="fReplacedRadio">                        <-------------- The container span that handles clicking
		<span class="fReplacedRadioImg"></span>          <-------------- This is a span that has the image
		<label for="delete">Delete</label>               <-------------- Label with for attribute
	</span>

Once the radio is clicked/selected a 'selected' class is added. This way you can customize everything with css.

	Selected radio

	<span class="fReplacedRadio selected">
		(...)
	</span>

If the original element has disabled="disabled" attribute, the class .disabled is appended

	Disabled checkbox

	<span class="fReplacedCheckbox disabled">
		(...)
	</span>

Handling clicks and jQuery data is done the same way, see 'Anatomy of a Checkbox' section for details.

***********************
* Anatomy of a Select *
***********************

A select element can actually be of two types, depending on the supplied 'multiple' attribute (or its absence).


* Select *

	Normal select element allows only one option to be selected at a time,
	and has all the options in a form of a sliding list.

	HTML code - Old

		<select id="someID" name="someName">
			<option value="value1">Value 1</option>
			<option value="value2">Value 2</option>
		</select>

							| |
							| |
							V V

	HTML code - Replaced

		<span class="fReplacedSelect">Value 1</span>

		<ul class="fReplacedSelectOptions">
			<li>Value 1</li>
			<li>Value 2</li>
		</ul>

		Note: the UL is hidden and only shown when clicking on .fReplacedSelect.

	Select with disabled attributes

		You can either disable the whole element, or just some options. The choice is yours!

		<select id="someID" name="someName" disabled="disabled">
			<option value="value1" disabled="disabled">Value 1</option>
			(...)
		</select>
							| |
							| |
							V V

		<span class="fReplacedSelect" disabled>Value 1</span>

		<ul class="fReplacedSelectOptions disabled">
			<li>Value 1</li>
			(...)
		</ul>



* Select - With Opt-Groups *


	HTML code - Old

		<select id="someID" name="someName">
			<optgroup label="Values 1 - 2">
				<option value="value1">Value 1</option>
				<option value="value2">Value 2</option>
			</optgroup>
			<optgroup label="Values 3 - 4">
				<option value="value3">Value 3</option>
				<option value="value4">Value 4</option>
			</optgroup>
		</select>

							| |
							| |
							V V

	HTML code - Replaced

		<span class="fReplacedSelect">Value 1</span>

		<ul class="fReplacedSelectOptions">
			<li class="fReplacedOptGroup">Values 1 - 2</li>
			<li>Value 1</li>
			<li>Value 2</li>
			<li class="fReplacedOptGroup">Values 3 - 4</li>
			<li>Value 3</li>
			<li>Value 4</li>
		</ul>

	Select - With Opt Groups - Disabled
		Same things apply. See * Select * to see how disabled attributes are handled

	jQuery Data

		Data behaves the same as in checkboxes/radios:
		'name' is applied to both span.select and ul.fReplacedSelectOptions
		Each LI inside ul.fReplacedSelectOptions has 'value' assigned with .data()



* Select - Multiple *


	This type of select has all of its options shown and allows for selecting multiple of them,
	either by holding CTRL and clicking, or selecting a range with Shift - Click.

	HTML code - Old

		<select id="someID" name="someName" multiple="multiple">
			<option value="value1">Value 1</option>
			<option value="value2">Value 2</option>
		</select>

							| |
							| |
							V V

	HTML code - Replaced

		<ul class="fReplacedSelectMultiple">
			<li>Value 1</li>
			<li>Value 2</li>
		</ul>

		When the option/LI is clicked/selected we add 'selected' class to it:

		<ul class="fReplacedSelectMultiple">
			<li>Value 1</li>
			<li class="selected">Value 2</li>
		</ul>


	Select - With Opt Groups - Disabled
		Same things apply. See * Select * to see how disabled attributes are handled
		
	Clicking

		Clicking behaves exactly like on the old select-multiple.
		You have to hold CTRL to select more than one option.

	Shift-Clicking

		You can select any range of options by holding Shift while clicking, just like on the normal element!



****************************
* Additional Functionality *
****************************

* Keyboard Navigation *
	Keyboard navigation is fully supported on the replaced elements, and behaves like the native one.

	Select/focus elements with tab:
		You can move around elements with tab. Pressing tab selects the next element (form or with tabindex assigned).
		Pressing Shift-Tab selects previous element.

		Tabindexes are carried over from the old elements to the new, and their numbering continues, so that the order remains the same.

	Arrow Keys:
		They work like they are supposed to.
		On Select, and Select-Multiple the Arrow Down & Arrow Right selects next options, Arrow Up & Arrow Left select previous.

	Shift/Ctrl:
		They provide advanced functionality on Select-Multiple element. Holding shift wile moving the arrows selects a range.
		Holding CTRL moves an outline which they you can toggle by clicking space.


	NOTE:
		Safari 3 has some serious problems with augmented keyboard navigation and focusing.
		It does not support tabindex attribute on non-form elements (like spans, uls and lis).
		I decided not to fix this, as it would require A LOT of time and code that is pretty much an unnecessary hack.

* RTL *
	Steps you need to perform:

	1. Set dir attribute on BODY tag:
		Like so: <body dir="rtl">(...)</body>

		This is very important. It is the proper way to indicate 'text' direction (on the whole page). Browsers take care of
		a lot of the things for you (like actual text reversion).

	2. Include RTL stylesheet:
		Like so:
			<link rel="stylesheet" href="style.css" />
			<link rel="stylesheet" href="style.rtl.css" />

		The order is important, RTL after normal stylesheet! Because the RTL only overwrites necessary elements.

	3. Pass the RTL option to the formReplacer plugin:
		Like so:
		    $(selector).formReplacer({rtl: true});

	This is all you need!


* Check/Uncheck All for checkboxes *

	This is a popular feature that allows you to check or uncheck all the checkboxes (in one group) with a single click.

	In order to enable this functionality you just have to add 'checkAll' class to any element, that will serve as the trigger.

	Then you add names of checkboxes you want to trigger, as additional classes!

	Example: 2 checkbox groups with labels
			(We use old HTML code [this is replaced by the plugin] just because it's easier to see attributes)

		<input type="checkbox" name="cars" id="car" value="car" />
		<label for="car">Car</label>
		<input type="checkbox" name="cars" id="car2" value="car2" />
		<label for="car2">Car 2</label>
		<input type="checkbox" name="cars" id="car3" value="car3" />
		<label for="car3">Car 3</label>

		<input type="checkbox" name="boats" id="boat" value="boat" />
		<label for="boat">Boat</label>
		<input type="checkbox" name="boats" id="boat2" value="boat2" />
		<label for="boat2">Boat 2 </label>
		<input type="checkbox" name="boats" id="boat3" value="boat3" />
		<label for="boat3">Boat 3</label>

		Clicking on this link will check all the checkboxes with name = 'cars':

		<a href="#" class="checkAll cars">Check all cars!</a>

		Clicking on this link will check all the checkboxes with name = 'cars'" AND those with name = 'boats':

		<a href="#" class="checkAll cars boats">Check all cars and boats!</a>

		Clicking on this link will only check the checkboxes with name = 'boats':

		<a href="#" class="checkAll boats">Check all boats!</a>


		Remember that you can use any element to trigger this behavior, divs, buttons - your imagination is the limit.
		Each consecutive click, will toggle the checked state of all matched checkboxes.
